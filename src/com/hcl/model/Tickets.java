package com.hcl.model;

public class Tickets {
	private Integer ticket_no;
	private String Holder_name;
	private long mobile_no;
	private Integer seat_no;
	private String movie_name;
public Tickets() {
	// TODO Auto-generated constructor stub
}
	public Tickets(Integer ticket_no, String Holder_name, long mobile, Integer seat_no,
			String movie_name) {
		super();
		this.ticket_no = ticket_no;
		this.Holder_name = Holder_name;
		this.mobile_no = mobile;
		this.seat_no = seat_no;
		this.movie_name = movie_name;
	}
	public Integer getTicket_no() {
		return ticket_no;
	}

	public void setTicket_no(Integer ticket_no) {
		this.ticket_no = ticket_no;
	}

	public String getHolder_name() {
		return Holder_name;
	}

	public void setHolder_name(String Holder_name) {
		this.Holder_name = Holder_name;
	}

	public long getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(long mobile_no) {
		this.mobile_no = mobile_no;
	}

	public Integer getSeat_no() {
		return seat_no;
	}

	public void setSeat_no(Integer seat_no) {
		this.seat_no = seat_no;
	}

	public String getMovie_name() {
		return movie_name;
	}

	public void setMovie_name(String movie_name) {
		this.movie_name = movie_name;
	}
}
		

	