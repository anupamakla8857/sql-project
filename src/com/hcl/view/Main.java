package com.hcl.view;

import java.util.List;
import java.util.Scanner;

import com.hcl.controller.AdminController;
import com.hcl.model.Tickets;

public class Main {

  public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    System.out.println("Enter user uid");
    String uid = s.nextLine();
    System.out.println("Enter user password");
    String password = s.nextLine();
    AdminController controller = new AdminController();
    int result = 0;
    result = controller.adminAuthentication(uid, password);
    if (result > 0) {
      System.out.println("Hi " + uid + ", Welcome to Admin Page");
    
      int con = 0;
     
      do {
        System.out.println(
            "1) Add new Tickets 2) Remove Tickets 3) Edit Tickets 4) View All Tickets");
        int option = s.nextInt();
        int ticket_no = 0;
        String Holder_name = "";
        long mobile_no =0;
        int seat_no=0;
        String movie_name = "";
     
        switch (option) {
        case 1:
          System.out.println("Enter ticket_no");
          ticket_no = s.nextInt();
          s.nextLine();
          System.out.println("Enter Holder_name");
          Holder_name = s.nextLine();
          System.out.println("Enter mobile_no");
          mobile_no = s.nextLong();
          s.nextLine();
          System.out.println("Enter seat_no");
          seat_no= s.nextInt();
          s.nextLine();
          System.out.println("Enter movie_name");
          movie_name = s.nextLine();
          result = controller.addTickets(ticket_no,Holder_name,mobile_no,seat_no,movie_name);
          if (result > 0) {
            System.out.println(ticket_no + " Successfully inserted");
          }
          break;
        case 2:
          System.out.println("Enter ticket_no remove from database");
          ticket_no = s.nextInt();
          s.nextLine();
          result = controller.removeTickets(ticket_no);

          System.out.println((result > 0) ? ticket_no + " remove was not found "
              : ticket_no + " Remove Successfully ");

          break;
        case 3:
          System.out.println("Enter ticket_no");
          ticket_no = s.nextInt();
          
          s.nextLine();
         
          System.out.println("Enter movie_name");
          movie_name = s.nextLine();
          result = controller.editInfo(ticket_no,movie_name);
          if (result > 0) {
            System.out.println(ticket_no+ "  Successfully Updated");
          }
          break;
        case 4:
          List<Tickets> list = controller.viewAllTickets();
          if (list.size() > 0) {
            System.out.println("ticket_no, Holder_name, mobile_no, seat_no, movie_name");
            for (Tickets tickets : list) {
              System.out.println(tickets.getTicket_no() + "," + tickets.getHolder_name() + ","
                  + tickets.getSeat_no() + "," + tickets.getMobile_no() + ","
                  + tickets.getMovie_name());

            }

          } else {
            System.out.println("No records found");
          }
          break;
        default:
          System.out.println("invalid option");
        }
        System.out.println("Do you want to continue press 1");
        con = s.nextInt();
      } while (con == 1);
      System.out.println("done");
    } else  {
      System.out.println("Your user id or password invalid");
    }
    s.close();

  }

}
