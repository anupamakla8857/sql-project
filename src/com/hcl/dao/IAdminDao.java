package com.hcl.dao;

import java.util.List;

import com.hcl.model.Tickets;
import com.hcl.model.Admin;

public interface IAdminDao {
  public int adminAuthentication(Admin admin);

  public List<Tickets> viewAllTicketss();

  public int addTickets(Tickets info);

  public int editInfo(Tickets edit);

  public int removeTickets(Tickets info);

}
