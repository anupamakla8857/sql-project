package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.hcl.model.Admin;
import com.hcl.model.Tickets;
import com.hcl.util.Db;
import com.hcl.util.Query;

public class IAdminimpl implements IAdminDao {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int adminAuthentication(Admin admin) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.adminAuth);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Exception oocurs in Admin Authentication");
			//e.printStackTrace();
			

		} 

	

		return result;
	}

	// @Override
	public List<Tickets> viewAllTickets() {
		List<Tickets> list = new ArrayList<Tickets>();
		try {
			pst = Db.getConnection().prepareStatement(Query.viewALL);
			rs = pst.executeQuery();
			while (rs.next()) {
				Tickets info = new Tickets(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getInt(4), rs.getString(5));
				list.add(info);
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in view All Tickets");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
				rs.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return list;
	}

	// @Override
	public int addTickets(Tickets info) {
		int result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.addTickets);
			pst.setInt(1, info.getTicket_no());
			pst.setString(2, info.getHolder_name());
			pst.setLong(3, info.getMobile_no());
			pst.setInt(4, info.getSeat_no());
			pst.setString(5, info.getMovie_name());

			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in Add new Tickets");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}

		}

		return result;
	}

	// @Override
	public int editInfo(Tickets edit) {
		int result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editInfo);

			pst.setString(1, edit.getMovie_name());
			pst.setInt(2, edit.getTicket_no());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in edit tickets");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}

		}

		return result;
	}

	// @Override
	public int removeTickets(Tickets info) {
		try {
			pst = Db.getConnection().prepareStatement(Query.removeTickets);
			pst.setInt(1, info.getTicket_no());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occurs in Remove tickets");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return 0;
	}

	@Override
	public List<Tickets> viewAllTicketss() {
		// TODO Auto-generated method stub
		return null;
	}

}