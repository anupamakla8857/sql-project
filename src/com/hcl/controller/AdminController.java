package com.hcl.controller;

import java.util.List;

import com.hcl.dao.IAdminDao;
import com.hcl.dao.IAdminimpl;
import com.hcl.model.Admin;
import com.hcl.model.Tickets;

public class AdminController {
	int result;
	IAdminDao dao = new IAdminimpl();

	public int adminAuthentication(String uid, String password) {
		Admin admin = new Admin(uid, password);
		return dao.adminAuthentication(admin);
	}

	public List<Tickets> viewAllTickets() {
		return dao.viewAllTicketss();

	}

	public int addTickets(int ticket_no, String holder_name, long mobile_no, int seat_no, String movie_name) {
		Tickets info = new Tickets(ticket_no, holder_name, mobile_no, seat_no, movie_name);
		return dao.addTickets(info);
	}

	public int editInfo(int ticket_no, String movie_name) {
		Tickets edit = new Tickets();
		edit.setTicket_no(ticket_no);
		edit.setMovie_name(movie_name);
		return dao.editInfo(edit);

	}

	public int removeTickets(int ticket_no) {
		Tickets info = new Tickets();
		info.setTicket_no(ticket_no);
		return dao.removeTickets(info);

	}

}
